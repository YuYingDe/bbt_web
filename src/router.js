module.exports = function(router){
    router.map({
       '/': {
            component: require('./components/home.vue'),
            name: 'home'
        },
        '/reg': {
            component: require('./components/reg.vue'),
            name: 'reg'
        },
        '/erp': {
            component: require('./components/erp.vue'),
            name: 'erp'
        },
        '/product': {
            component: require('./components/product.vue'),
            name: 'product'
        },
        '/case': {
            component: require('./components/case.vue'),
            name: 'case'
        },
        '/about': {
            component: require('./components/about.vue'),
            name: 'about'
        }
    })
}