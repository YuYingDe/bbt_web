import navbarComponent from '../components/navbarComponent.vue';
import footerComponent from '../components/footerComponent.vue';
var isClick = true;
var countdown = 10;
var countdowning = true;
var countDownInterval = null;
module.exports = {
    data() {
        return {
            item: {},
            showCustomModal: false,
            time: '',
            distableButton: 'distable-button'
        }
    },
    components: {
        footerComponent,
        navbarComponent
    },
    methods: {
        showRule(){
            var self = this;
            this.showCustomModal = true;
            countDownInterval = setInterval(function(){
                if (countdown == 0) {
                    countdown = 10;
                    self.distableButton = 'close-button';
                    countdowning = false;
                    self.time = '关闭';
                    clearInterval(countDownInterval);
                }else{
                    countdown--;
                    countdowning = true;
                    self.distableButton = 'distable-button';
                    self.time = '关闭（' + countdown + '）';
                }
            }, 1000);
            // this.$nextTick(function(){
            //     setTimeout(function(){
            //         self.showCustomModal = false;
            //     }, 10000);
            // });
        },
        close(){
            if(!countdowning){
                this.showCustomModal = false;
            }
        },
        submit(){
            var item = this.item;
            if(item.name == null || item.name == ''){
                alert('请输入机构名称');
                return;
            }
            if(item.user == null || item.user == ''){
                alert('请输入联系人');
                return;
            }
            if(item.phone == null || item.phone == ''){
                alert('请输入联系电话');
                return;
            }
            if(!isClick){
                alert('请勿重复提交');
                return;
            }
            isClick = false;
            var self = this;
            var params = {
                m: 'reg',
                name: item.name,
                phone: item.phone,
                contact: item.user
            };
            console.log(this)
            this.$httpGet('org', params, function (code, data) {
                if (code == 0) {
                    alert('申请成功提交！' +  '\n' + '在1-3工作日审核，审核通过后将会有人与你联系，请保持手机畅通！')
                } else {
                    alert('error!')
                }
                self.item = {};
                isClick = true;
            });
        }
    }
};