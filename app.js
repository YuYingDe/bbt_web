/**
 * Created by Kevin on 4/6/15.
 */
'use strict';
var ports = 9090;
var express = require('express');
var app = express();
app.set('port', process.env.PORT || ports);

app.use(express.static(__dirname + '/'));

app.use(function(req, res){
    //res.type('text/plain');
    //res.status(404);
    //res.send('404 - Not Found');
    res.sendfile('index.html');
});

app.use(function(req, res){
    res.type('text/plain');
    res.status(500);
    res.send('500 - Server Error');
});

app.listen(app.get('port'), function(err){
    console.log('Express started on http://localhost:' + ports);
});